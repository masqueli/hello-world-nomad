job "php-worker-service" {
  
  type = "service"
  datacenters = ["cristal"]
  
  meta {
    git_sha = "[[${CI_COMMIT_SHORT_SHA}]]"
  }
  
  group "php-fpm" {
    count = 1

    network {

      mode = "host"
      port "php-fpm" {
        to = 9000
        static = 9000
      }

    } # end of network decl. for php-fpm

    task "php-fpm" {

      artifact {
        source = "git::http://gitlab-backend.priv.lifl.fr/masqueli/hello-world-nomad.git"
        destination = "local/datas/"
      }

      driver = "docker"

      config {
        image = "php:7-fpm"
        ports = ["php-fpm"]
        # volumes = [ "local/datas/index.html:/usr/share/nginx/html/index.html", "local/datas/mesinfos.php:/usr/share/nginx/html/mesinfos.php" ]
      }

      service {
        name = "php-fpm"
        port = "php-fpm"
        provider = "consul"
      }

    } # end of task decl. for php-fpm

  }
  
  group "whoami" {
    count = 1
    
    network {

      mode = "host"
      port "webserver" {
        to = 80
      }

    } # end of network decl. for php-fpm

    task "server" {

      artifact {
        source = "git::http://gitlab-backend.priv.lifl.fr/masqueli/hello-world-nomad.git"
        destination = "local/datas/"
      }

      driver = "docker"
   
      config {
        image = "nginx"
        ports = ["webserver"]
        volumes = [ "local/datas/index.html:/usr/share/nginx/html/index.html", "local/default.conf:/etc/nginx/conf.d/default.conf", "local/datas/mesinfos.php:/usr/share/nginx/html/mesinfos.php" ]
      }

      resources {
        cpu = 128  # 128 Mhz
        memory = 128 # 128MB
      }

      service {
        name = "whoami"
        port = "webserver"
        provider = "consul"

        check {
          name = "host-whoami-check"
          type = "tcp"
          interval = "10s"
          timeout = "2s"
        }

        tags = [
          "traefik.enable=true",
          "traefik.http.services.whoami.loadbalancer.sticky",
          "traefik.http.routers.whoami.rule=Host(`whoami.priv.lifl.fr`)",
        ]
      } # end of service decl. for nginx

      template {
        change_mode = "restart"
        destination = "local/default.conf"
        data = <<EOH
server {
  listen 80; 
  server_name  localhost;
  index index.php index.html;

  root   /local/datas;

  charset utf-8;
  #access_log  /var/log/nginx/log/host.access.log  main;

  #error_page  404              /404.html;

  # redirect server error pages to the static page /50x.html
  #
  error_page   500 502 503 504  /50x.html;
  location = /50x.html {
    root   /usr/share/nginx/html;
  }

  # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
  location ~ \.php$ {
    try_files $uri =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass {{ range service "php-fpm" }}{{ .Address }}:{{ .Port }}{{ end }};
    fastcgi_index index.php;
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param PATH_INFO $fastcgi_path_info;
  }

  # deny access to .htaccess files, if Apache's document root
  # concurs with nginx's one
  #
  #location ~ /\.ht {
  #    deny  all;
  #}
}   
EOH
      } # end of template decl. for nginx

    } # end of task decl. for nginx
  }
}

